# Avatar Returns - AvatarHouse

## Présentation

<p>➠  Type : Module </p>
<p>➠  Développeur(s) : BakaAless</p>
<p>➠  État : En cours... </p>


## Description

➠  Ce plugin est un module d'[AvatarCore](https://gitlab.com/avatar-tales/avatar-core) permettant de créer des maisons
pouvant être louées.
