package fr.avatarreturns.avatarhouse.plugin;

import fr.avatarreturns.api.AvatarPlugin;
import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarhouse.commands.CommandManager;
import fr.avatarreturns.avatarhouse.configuration.Config;
import fr.avatarreturns.avatarhouse.listeners.Listening;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.permissions.Permissions;
import fr.avatarreturns.avatarhouse.system.House;
import fr.avatarreturns.avatarhouse.system.Neighbourhood;
import fr.avatarreturns.avatarhouse.utils.Scheduler;

import java.io.File;
import java.util.Optional;

public class AvatarHouse extends AvatarPlugin {

    private String houseSavePath;
    private String neighborhoodSavePath;

    private boolean canSpawn = false;

    public static AvatarHouse get() {
        return (AvatarHouse) getProvidingPlugin(AvatarHouse.class);
    }

    @Override
    public void onEnable() {
        super.onEnable();
        if (!AvatarReturnsAPI.get().isIntegrate("Vault")) {
            System.out.println("Vault isn't integrated !");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        if (!AvatarReturnsAPI.get().isIntegrate("WorldGuard")) {
            System.out.println("WorldGuard isn't integrated !");
            this.getServer().getPluginManager().disablePlugin(this);
            return;
        }
        Config.getInstance().init(this.getDataFolder().getAbsolutePath(), "config.yml");
        Messages.init(this.getDataFolder().getAbsolutePath(), "messages.yml");
        Permissions.init(this.getDataFolder().getAbsolutePath(), "permissions.yml");
        final File neighborhoodSaveFile = new File(this.getDataFolder().getAbsolutePath(), "neighbourhoods");
        if (!neighborhoodSaveFile.exists())
            neighborhoodSaveFile.mkdirs();
        this.neighborhoodSavePath = neighborhoodSaveFile.getAbsolutePath();
        final File houseSaveFile = new File(this.getDataFolder().getAbsolutePath(), "houses");
        if (!houseSaveFile.exists())
            houseSaveFile.mkdirs();
        this.houseSavePath = houseSaveFile.getAbsolutePath();
        this.getServer().getScheduler().runTask(this, () -> {
            for (final Optional<Neighbourhood> neighbourhoodOptional : Neighbourhood.load(this.neighborhoodSavePath)) {
                if (!neighbourhoodOptional.isPresent())
                    continue;
                Neighbourhood.getNeighbourhoods().add(neighbourhoodOptional.get());
                neighbourhoodOptional.get().generateEntity();
            }
            this.canSpawn = true;
            House.load(this.houseSavePath).forEach(optionHouse -> optionHouse.ifPresent(house -> {
                House.getHouses().add(house);
                house.updateSign();
                house.updateRegions();
            }));
        });
        AvatarReturnsAPI.get().registerCommandClass(new CommandManager());
        this.getServer().getPluginManager().registerEvents(new Listening(), this);
        AvatarReturnsAPI.get().registerUpdated(Scheduler.get());
    }

    @Override
    public void onDisable() {
        super.onDisable();
        AvatarReturnsAPI.get().unregisterUpdated(Scheduler.get());
        this.canSpawn = false;
        for (final Neighbourhood neighbourhood : Neighbourhood.getNeighbourhoods()) {
            neighbourhood.removeEntity();
            neighbourhood.save();
        }
        for (final House house : House.getHouses())
            house.save();
    }

    public String getHouseSavePath() {
        return this.houseSavePath;
    }

    public String getNeighborhoodSavePath() {
        return this.neighborhoodSavePath;
    }

    public boolean isCanSpawn() {
        return this.canSpawn;
    }
}
