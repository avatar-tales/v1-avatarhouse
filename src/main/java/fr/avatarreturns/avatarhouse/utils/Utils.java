package fr.avatarreturns.avatarhouse.utils;

import com.sk89q.worldedit.bukkit.BukkitAdapter;
import com.sk89q.worldguard.WorldGuard;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;
import com.sk89q.worldguard.protection.regions.RegionContainer;
import net.minecraft.server.v1_16_R3.PacketPlayOutNamedSoundEffect;
import net.minecraft.server.v1_16_R3.SoundCategory;
import net.minecraft.server.v1_16_R3.SoundEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_16_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.stream.Collectors;

public class Utils {

    public static boolean isRegion(final World world, final String name) {
        final RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        return container.get(BukkitAdapter.adapt(world)).getRegion(name) != null;
    }

    @Nullable
    public static ProtectedRegion getRegion(final World world, final String name) {
        final RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        return container.get(BukkitAdapter.adapt(world)).getRegion(name);
    }

    @Nullable
    public static List<String> getRegions(final World world) {
        final RegionContainer container = WorldGuard.getInstance().getPlatform().getRegionContainer();
        return container.get(BukkitAdapter.adapt(world)).getRegions().values().stream().map(ProtectedRegion::getId).collect(Collectors.toList());
    }

    public static String change(double i) {
        int n2;
        StringTokenizer t = new StringTokenizer(String.valueOf(Math.ceil(i * 100) / 100), ".");
        String s1 = t.nextToken();
        String s2 = t.nextToken();
        n2 = s2.length();
        if (n2 != 1) {
            if (s2.charAt(s2.length() - 1) == '0') n2 = s2.length() - 1;
        }
        if (n2 == 0 || (n2 == 1 && s2.charAt(0) == '0')) {
            return s1;
        }
        return i + "";
    }

    public static boolean isSign(final Material m) {
        return m.name().toLowerCase().contains("sign");
    }

    /**
     * Code par SamaGames
     * <p>
     * https://github.com/SamaGames/SurvivalAPI/blob/master/src/main/java/net/samagames/survivalapi/game/SurvivalGameLoop.java#L330
     **/
    public static String getDirection(final Location start, final Location end) {
        Location ploc = start.clone();
        Location point = end.clone();

        if (ploc.getWorld().getEnvironment() != point.getWorld().getEnvironment())
            return "•";

        ploc.setY(0);
        point.setY(0);

        Vector d = ploc.getDirection();
        Vector v = point.subtract(ploc).toVector().normalize();

        double a = Math.toDegrees(Math.atan2(d.getX(), d.getZ()));
        a -= Math.toDegrees(Math.atan2(v.getX(), v.getZ()));
        a = (int) (a + 22.5) % 360;

        if (a < 0)
            a += 360;

        return Character.toString("⬆⬈➡⬊⬇⬋⬅⬉".charAt((int) a / 45));
    }

    public static String fromDuration(final long timeStamp) {
        final int millis = (int) Math.ceil(timeStamp / 1000D);
        final int seconds = (int) Math.ceil(millis % 60D);
        final int minutes = (int) Math.floor((millis / 60D) % 60D);
        final int hours = (int) Math.floor((millis / 3600D) % 24D);
        final int days = (int) Math.floor((millis / 3600D / 24D) % 30D);
        final int months = (int) Math.floor((millis / 3600D / 24D / 30D) % 12D);
        final int years = (int) Math.floor(millis / 3600D / 24D / 30D / 12D);
        final StringBuilder sb = new StringBuilder();
        if (years > 0)
            sb.append(years).append(" an").append(years > 1 ? "s" : "").append(isNotNull(1, months, days, hours, minutes, seconds) ? " et " : (!isNotNull(0, months, days, hours, minutes, seconds) ? ", " : ""));
        if (months > 0)
            sb.append(months).append(" mois").append(isNotNull(1, days, hours, minutes, seconds) ? " et " : (!isNotNull(0, days, hours, minutes, seconds) ? ", " : ""));
        if (days > 0)
            sb.append(days).append(" jour").append(days > 1 ? "s" : "").append(isNotNull(1, hours, minutes, seconds) ? " et " : (!isNotNull(0, hours, minutes, seconds) ? ", " : ""));
        if (hours > 0)
            sb.append(hours).append(" heure").append(hours > 1 ? "s" : "").append(isNotNull(1, minutes, seconds) ? " et " : (!isNotNull(0, minutes, seconds) ? ", " : ""));
        if (minutes > 0)
            sb.append(minutes).append(" minute").append(minutes > 1 ? "s" : "").append(isNotNull(1, seconds) ? " et " : "");
        if (seconds > 0)
            sb.append(seconds).append(" seconde").append(seconds > 1 ? "s" : "");
        if (sb.length() == 0)
            sb.append("maintenant");
        return sb.toString();
    }

    private static boolean isNotNull(final int numbers, final int... integers) {
        return Arrays.stream(integers).asDoubleStream().filter(number -> number != 0).count() == numbers;
    }

    public static void playSound(final Player player, SoundEffect sound) {
        final PacketPlayOutNamedSoundEffect packet = new PacketPlayOutNamedSoundEffect(sound, SoundCategory.MASTER, player.getLocation().getX(), player.getLocation().getY(), player.getLocation().getZ(), 100, 1);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);
    }

}
