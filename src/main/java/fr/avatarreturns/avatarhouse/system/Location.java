package fr.avatarreturns.avatarhouse.system;

import org.bukkit.Bukkit;

public class Location {

    private String world;
    private double x;
    private double y;
    private double z;

    private float pitch;
    private float yaw;

    private Location() {
    }

    public static Location convert(final org.bukkit.Location loc) {
        final Location location = new Location();
        location.world = loc.getWorld().getName();
        location.x = loc.getX();
        location.y = loc.getY();
        location.z = loc.getZ();
        location.yaw = loc.getYaw();
        location.pitch = loc.getPitch();
        return location;
    }

    public static org.bukkit.Location convert(final Location loc) {
        return new org.bukkit.Location(Bukkit.getWorld(loc.world), loc.x, loc.y, loc.z, loc.yaw, loc.pitch);
    }

    public String getWorld() {
        return world;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public float getPitch() {
        return pitch;
    }

    public float getYaw() {
        return yaw;
    }

    public org.bukkit.Location convert() {
        return convert(this);
    }
}
