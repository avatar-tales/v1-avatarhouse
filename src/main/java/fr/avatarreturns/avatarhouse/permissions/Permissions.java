package fr.avatarreturns.avatarhouse.permissions;

import fr.avatarreturns.avatarhouse.configuration.Config;
import fr.avatarreturns.avatarhouse.messages.Messages;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;

public enum Permissions {

    NONE(""),
    ADMIN("avatar.admin"),

    CREATE("avatar.houses.create"),
    KICK("avatar.houses.kick"),
    EDIT("avatar.houses.edit");

    private String permission;

    Permissions(final String permission) {
        this.permission = permission;
    }

    public static boolean hasPermission(final CommandSender sender, final Permissions permission) {
        final String[] element = permission.permission.split(".");
        for (int index = 0; index < element.length - 1; index++) {
            final StringBuilder permissions = new StringBuilder();
            permissions.append(element[index]);
            for (int i = 0; i < index; i++) {
                permissions.append(".").append(element[i + 1]);
            }
            permissions.append(".*");
            if (sender.hasPermission(permissions.toString()))
                return true;
        }
        return sender.hasPermission(Permissions.ADMIN.permission) || sender.hasPermission(permission.permission);
    }

    public String getPermission() {
        return permission;
    }

    public boolean hasPermission(final CommandSender sender) {
        return hasPermission(sender, this);
    }

    public static void init(final String path, final String name) {
        final File file = new File(path, name);
        try {
            if (!file.exists()) {
                file.getParentFile().mkdirs();
                file.createNewFile();
            }
            final YamlConfiguration config = YamlConfiguration.loadConfiguration(file);
            for (final Permissions permissions : Permissions.values()) {
                if (permissions.permission.equals(""))
                    continue;
                final String pathToMessage = permissions.name().replace("__", "-").replace("_", ".").toLowerCase();
                if (!Config.addDefault(config, pathToMessage, permissions.permission)) {
                    permissions.permission = config.getString(pathToMessage);
                }
            }
            config.save(file);
        } catch (Exception ignored) {
        }
    }

}
