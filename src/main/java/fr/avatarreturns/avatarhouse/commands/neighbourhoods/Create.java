package fr.avatarreturns.avatarhouse.commands.neighbourhoods;

import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.system.Neighbourhood;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Create {

    public static void handle(final CommandSender sender, final List<String> arguments) {
        if (Neighbourhood.getNeighbourhoods().stream().anyMatch(neighbourhood -> neighbourhood.getName().equals(arguments.get(0)))) {
            return;
        }
        final Neighbourhood neighbourhood = new Neighbourhood(arguments.get(0));
        Neighbourhood.getNeighbourhoods().add(neighbourhood);
        sender.sendMessage(Messages.INFO_UPDATED.get());
    }

}
