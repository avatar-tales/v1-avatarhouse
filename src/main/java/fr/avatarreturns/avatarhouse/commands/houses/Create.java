package fr.avatarreturns.avatarhouse.commands.houses;

import fr.avatarreturns.api.AvatarReturnsAPI;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.permissions.Permissions;
import fr.avatarreturns.avatarhouse.system.House;
import org.bukkit.command.CommandSender;

import java.util.List;

public class Create {

    public static void handle(final CommandSender sender, final List<String> arguments) {
        if (House.getHouses().stream().anyMatch(house -> house.getName().equals(arguments.get(0)))) {
            return;
        }
        final House house = new House(arguments.get(0));
        House.getHouses().add(house);
        sender.sendMessage(Messages.INFO_UPDATED.get());
    }

}
