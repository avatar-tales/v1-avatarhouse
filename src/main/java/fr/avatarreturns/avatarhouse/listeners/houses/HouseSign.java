package fr.avatarreturns.avatarhouse.listeners.houses;

import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.permissions.Permissions;
import fr.avatarreturns.avatarhouse.system.House;
import fr.avatarreturns.avatarhouse.system.Location;
import fr.avatarreturns.avatarhouse.utils.Utils;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Directional;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.Optional;

public class HouseSign {

    public static void onBlockBreak(final BlockBreakEvent e) {
        Optional<Location> location;
        for (final House house : House.getHouses()) {
            location = house.getSign();
            if (location.isPresent()) {
                if (e.getBlock().getLocation().equals(location.get().convert())) {
                    e.setCancelled(true);
                    return;
                } else {
                    if (!Utils.isSign(location.get().convert().getBlock().getType()))
                        continue;
                    final Sign sign = (Sign) location.get().convert().getBlock().getState();
                    final BlockData data = sign.getBlockData();
                    if (data instanceof Directional) {
                        final Directional directional = (Directional) data;
                        final Block blockBehind = sign.getBlock().getRelative(directional.getFacing().getOppositeFace());
                        if (e.getBlock().getLocation().equals(blockBehind.getLocation())) {
                            e.setCancelled(true);
                            return;
                        }
                    }
                }
            }
        }
    }

    public static void interact(final PlayerInteractEvent e) {
        Optional<Location> location;
        for (final House house : House.getHouses()) {
            location = house.getSign();
            if (location.isPresent()) {
                if (e.getClickedBlock().getLocation().equals(location.get().convert())) {
                    e.setCancelled(true);
                    if (e.getPlayer().isSneaking() && Permissions.EDIT.hasPermission(e.getPlayer())) {
                        House.Inventories.editHouseMenu(house, e.getPlayer(), 1);
                        return;
                    }
                    if (!house.getOwner().isPresent()) {
                        e.getPlayer().sendMessage(Messages.INFO_GOTO.get());
                    }
                }
            }
        }
    }

}
