package fr.avatarreturns.avatarhouse.utils;

import fr.avatarreturns.api.schedulers.IUpdated;
import fr.avatarreturns.avatarhouse.messages.Messages;
import fr.avatarreturns.avatarhouse.system.House;
import org.bukkit.Bukkit;

import java.util.stream.Collectors;

public class Scheduler implements IUpdated {

    private static Scheduler instance;

    public static Scheduler get() {
        if (instance == null)
            instance = new Scheduler();
        return instance;
    }

    private int step;

    private Scheduler() {
        this.step = 0;
    }

    @Override
    public void run() {
        if (step++ % 20 == 0) {
            for (final House house : House.getHouses().stream().filter(house -> house.getState().equals(House.State.OCCUPIED)).collect(Collectors.toList())) {
                if (house.getLastRentTime() + house.getRentTime() < System.currentTimeMillis()) {
                    house.setOwner(null);
                }
            }
        }
    }

}
