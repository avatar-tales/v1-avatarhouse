package fr.avatarreturns.avatarhouse.configuration;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

public class Config {

    private static transient Config instance;
    private static int house_default_price = 20000;
    private static int house_default_rent = 5000;
    private static long house_default_rentTime = 2 * 7 * 24 * 60 * 60 * 1000;
    private static double house_default_coowner = 750;
    private static double house_default_member = 500;
    private static double house_default_strangerToMember = 1000;
    private static double house_default_memberToCoOwner = 750;
    private static double house_default_coOwnerToMember = 0;
    private static double house_default_memberToStranger = 0;
    private static double house_default_leave = 0;
    private static boolean house_default_reset = false;
    private static int house_max_coowners = 1;
    private static int house_max_members = 4;
    private static int house_range_search_player = 10;
    private static int neighbourhood_max_houses = 2;
    private static String neighbourhood_default_name = "&6&lAgent Immobilier";
    private transient File file;
    private transient YamlConfiguration config;

    public static Config getInstance() {
        if (instance == null)
            instance = new Config();
        return instance;
    }

    public static boolean addDefault(final YamlConfiguration config, final String path, final Object o) {
        if (config.get(path) == null) {
            config.set(path, o);
            return true;
        }
        return false;
    }

    public static int getHouse_default_price() {
        return house_default_price;
    }

    public static int getHouse_default_rent() {
        return house_default_rent;
    }

    public static long getHouse_default_rentTime() {
        return house_default_rentTime;
    }

    public static boolean isHouse_default_reset() {
        return house_default_reset;
    }

    public static double getHouse_default_coowner() {
        return house_default_coowner;
    }

    public static double getHouse_default_member() {
        return house_default_member;
    }

    public static double getHouse_default_strangerToMember() {
        return house_default_strangerToMember;
    }

    public static double getHouse_default_memberToCoOwner() {
        return house_default_memberToCoOwner;
    }

    public static double getHouse_default_coOwnerToMember() {
        return house_default_coOwnerToMember;
    }

    public static double getHouse_default_memberToStranger() {
        return house_default_memberToStranger;
    }

    public static double getHouse_default_leave() {
        return house_default_leave;
    }

    public static int getNeighbourhood_max_houses() {
        return neighbourhood_max_houses;
    }

    public static int getHouse_max_coowners() {
        return house_max_coowners;
    }

    public static int getHouse_max_members() {
        return house_max_members;
    }

    public static int getHouse_range_search_player() {
        return house_range_search_player;
    }

    public static String getNeighbourhood_default_name() {
        return ChatColor.translateAlternateColorCodes('&', neighbourhood_default_name);
    }

    public void init(final String path, final String name) {
        this.file = new File(path, name);
        try {
            if (!this.file.exists()) {
                this.file.getParentFile().mkdirs();
                this.file.createNewFile();
            }
            this.config = YamlConfiguration.loadConfiguration(file);
            for (final Field field : Config.class.getDeclaredFields()) {
                if (Modifier.isTransient(field.getModifiers()))
                    continue;
                final String pathField = field.getName().replace("_", ".");
                System.out.println("pathField = " + pathField);
                if (!addDefault(this.config, pathField, field.get(this)) && !Modifier.isFinal(field.getModifiers()))
                    field.set(this, this.config.get(pathField));
                else if (Modifier.isFinal(field.getModifiers()))
                    this.config.set(pathField, field.get(this));
            }
            this.config.save(this.file);
        } catch (IOException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
